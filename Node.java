class Node {
    int iData;
    Node leftChild;
    Node rightChild;
    double dData;

    public Node(int iData) {
        this.iData = iData;
        this.dData = dData;
        leftChild = null;
        rightChild = null;
    }

    public Node() {
        // Empty constructor
    }
}
