class Tree {
    public Node root; // The only data field in Tree

    // Constructor for the Tree class can be added here if needed.

    // Method to find a node with a given key
    public Node find(int key) {
        // Start at the root
        Node current = root;
    
        // Traverse the tree until a match is found or we reach the end
        while (current.iData != key) {
            if (key < current.iData) {
                // Go left if the key is smaller
                current = current.leftChild;
            } else {
                // Go right if the key is larger
                current = current.rightChild;
    
                if (current == null) {
                    // If no child is found, the key doesn't exist in the tree
                    return null;
                }
            }
        }
    
        // Return the node with the given key (when found)
        return current;
    }
    
    // Method to insert a node with an ID and a double value
    public void insert(int id, double dd) {
        Node newNode = new Node(); // Create a new node
        newNode.iData = id; // Insert data
        newNode.dData = dd;
    
        if (root == null) { // If the tree is empty, make the new node the root
            root = newNode;
        } else {
            Node current = root; // Start at the root
            Node parent;
    
            while (true) { // Continue until the new node is inserted
                parent = current;
    
                if (id < current.iData) { // Go left?
                    current = current.leftChild;
                    if (current == null) { // If there are no more nodes on the left, insert the new node there
                        parent.leftChild = newNode;
                        return;
                    }
                } else { // Go right
                    current = current.rightChild;
                    if (current == null) { // If there are no more nodes on the right, insert the new node there
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }
    private Node getSuccessor(Node delNode) {
        Node successorParent = delNode;
        Node successor = delNode.rightChild;
        Node current = delNode.rightChild;
    
        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.leftChild;
        }
    
        if (successor != delNode.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
    
        return successor;
    }
    

    // Method to delete a node with a given ID
    public boolean delete(int key) {
        Node current = root;
        Node parent = root;
        boolean isLeftChild = true;
    
        while (current.iData != key) {
            parent = current;
            if (key < current.iData) {
                isLeftChild = true;
                current = current.leftChild;
            } else {
                isLeftChild = false;
                current = current.rightChild;
            }
            if (current == null) {
                return false; // Didn't find the node to delete
            }
        }
    
        // If no children, simply delete it
        if (current.leftChild == null && current.rightChild == null) {
            if (current == root) {
                root = null; // Tree is empty
            } else if (isLeftChild) {
                parent.leftChild = null; // Disconnect from parent
            } else {
                parent.rightChild = null; // Disconnect from parent
            }
        }
        // If no right child, replace with left subtree
        else if (current.rightChild == null) {
            if (current == root) {
                root = current.leftChild;
            } else if (isLeftChild) {
                parent.leftChild = current.leftChild;
            } else {
                parent.rightChild = current.leftChild;
            }
        }
        // If no left child, replace with right subtree
        else if (current.leftChild == null) {
            if (current == root) {
                root = current.rightChild;
            } else if (isLeftChild) {
                parent.leftChild = current.rightChild;
            } else {
                parent.rightChild = current.rightChild;
            }
        }
        // Node to be deleted has two children
        else {
            Node successor = getSuccessor(current);
    
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parent.leftChild = successor;
            } else {
                parent.rightChild = successor;
            }
    
            successor.leftChild = current.leftChild;
        }
    
        return true;
    }
    

    public Node minimum() {
        Node current = root; // Start at the root
        Node last = null;   // Initialize last to null as a local variable
    
        while (current != null) { // Traverse until the bottom
            last = current; // Remember the current node
            current = current.leftChild; // Go to the left child
        }
    
        return last; // Return the node with the minimum key value
    }
    // Various other methods can be added here as needed
}
